# Elasticsearch y Kibana para recopilación de logs.

## Descripción

La idea de este proyecto es la de poder montar una herramienta de recopilación de log para su visualización y analisis utilizando elasticsearch y kibana, en los clientes utilizaremos los agentes de beats los cuales realizan la recopilación y parseo de los datos para que pueda ser interpretado por kibana.
Para su correcto funcionamiento vamos a necesitar tener instalado Docker y clonar el proyecto desde gitlab. 

El proyecto contiene 4 scripts en bash (startek.sh, updateelastic.sh, update-ek.sh, updatekibana.sh) y 2 dockerfiles (kibana, elasticsearch)

Los 4 scripts sirven para iniciar los dockers y actualizar las imagenes con las modificaciones desde gitlab. 

## Instalación Docker

**Linux (Debian)**

_Configuración de los repositorios_

1. Actualizar el indice de paquetes de "apt" y los paquetes de actualización para permitir apt para usar repositorios sobre HTTPS

```
$ sudo apt-get update

$ sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```
2. Agregar las llaves GPG de Docker oficial

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

3. Usar el siguiente comando para configurar un repositorio estable.

```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

_Instalación del motor de Docker_

1. Actualizar el indice de paquete de "apt", instalar la última versión del motor de Docker y containerd.

```
 $sudo apt-get update
 $sudo apt-get install docker-ce docker-ce-cli containerd.io
```

2. Verificar que Docker está correctamente instalado utilizando la imagen "hello world"

```
$sudo docker run hello-world
```


## Docker y Configuración de Elasticsearch, Kibana y Beats

0. **Dockers files**

- [ ] [elasticsearch](https://gitlab.com/adrian.guirao/elk/-/blob/3648709ae0ca3c394e54d8ad47e81322caf9fcaf/elasticsearch)

```
ARG elasticsearch_version="7.15.2-amd64"
FROM docker.elastic.co/elasticsearch/elasticsearch:${elasticsearch_version}
LABEL version="1"
VOLUME ["/usr/share/elasticsearch/data"]
# es_java_opts
# La cantidad de RAM deberá ser de al menos 2GB para testing, solo si se reciben algunos flows.
# Para ambientes productivos elevarlo hasta 31GB de RAM es recomendado.
ENV es_java_opts '-Xms2g -Xmx2g'
ENV cluster.name elastiflow
ENV elasticsearch_password elastic
# bootstrap.memory_lock si se habilita se bloquea la memoria asignada a la jvm para que no se sobrepase el consumo.
ENV bootstrap.memory_lock 'true'
ENV network.host 0.0.0.0
ENV http.port 9200
ENV discovery.type single-node

EXPOSE 9200
```

_Versión de la imagen:_
```
ARG elasticsearch_version="7.15.2-amd64"
```
- Al momento de crear la imagen podremos construirla con otra versión por medio del argumento "elasticsearch_version", por defecto se encuentra la última imagen estable de la fecha. El comando para ingresar el argumento el siguiente "--build-arg elasticsearch_version="version""

***

_Imagen del docker:_
```
FROM docker.elastic.co/elasticsearch/elasticsearch:${elasticsearch_version}
```
- Se selecciona la imagen oficial en el repositorio de elastic

***

_Etiqueta del dockerfile:_

```
LABEL version="1"
```
- La etiqueta sirve para organizar las imagenes al momento de construirlas

***

_Volumen:_

```
VOLUME ["/usr/share/elasticsearch/data"]
```
- Se persiste el volumen necesario para no perder datos al momento de bajar el docker

***

_Memoria ram para el JVM:_
```
ENV es_java_opts '-Xms2g -Xmx2g'
```
- La memoria RAM recomendada es de al menos 2GB para testing, solo si se reciben algunos flows. Para ambientes productivos elevarlo hasta 31GB de RAM es recomendado.

***

_Cluster elasticsearch:_

```
ENV cluster.name elastiflow
```
- Se define el nombre del cluster

***

_Definición de contraseña:_
```
ENV elasticsearch_password elastic
```
- Se configura el password de elasticsearch (Se recomienda cambiar la clave) para el acceso
***
_Bloqueo de memoria RAM:_

```
ENV bootstrap.memory_lock 'true'
```
- Se previene que el jvm no sobrepase el consumo.
***

_Interfaces de escucha:_
```
ENV network.host 0.0.0.0
```
- Se especifica en que interfaces va a estar escuchando el servicio
***
_Puerto de escucha:_
```
ENV http.port 9200
```
- Se especifica el puerto de escucha del servicio
***
_Tipo de descubrimiento:_
```
ENV discovery.type single-node
```
- Se configura si se va a utilizar solo un nodo o varios de elasticsearch. Para el caso de pruebas se configura como "single-node".
***
_Exponer puerto de docker:_

```
EXPOSE 9200
```
- Se expone el puerto de escucha del servicio de elasticsearch.

***
***

- [ ] [kibana](https://gitlab.com/adrian.guirao/elk/-/blob/3648709ae0ca3c394e54d8ad47e81322caf9fcaf/kibana)

```
ARG KIBANA_VERSION="7.15.2-amd64"
FROM docker.elastic.co/kibana/kibana:${KIBANA_VERSION}
LABEL version="1"

ENV SERVER_HOST "0.0.0.0"
ENV ELASTICSEARCH_HOSTS "http://127.0.0.1:9200"
ENV ELASTICSEARCH_USERNAME "elastic"
ENV ELASTICSEARCH_PASSWORD "elastic"

EXPOSE 5601

```

_Versión de la imagen:_
```
ARG KIBANA_VERSION="7.15.2-amd64"
```
- Al momento de crear la imagen podremos construirla con otra versión por medio del argumento "elasticsearch_version", por defecto se encuentra la última imagen estable de la fecha. El comando para ingresar el argumento el siguiente "--build-arg elasticsearch_version="version""
***

_Imagen del docker:_
```
FROM docker.elastic.co/kibana/kibana:${KIBANA_VERSION}
```
- Se selecciona la imagen oficial en el repositorio de elastic
***

_Etiqueta del dockerfile:_

```
LABEL version="1"
```
- La etiqueta sirve para organizar las imagenes al momento de construirlas

***

_Interfaces de escucha:_
```
ENV SERVER_HOST "0.0.0.0"
```
- Se especifica en que interfaces va a estar escuchando el servicio

***
_HOST de elasticsearch:_
```
ENV ELASTICSEARCH_HOSTS "http://127.0.0.1:9200"
```
- Se define el host elasticsearch para la lectura de los indices.

***
_Usuario y password de elasticsearch:_
```
ENV ELASTICSEARCH_USERNAME "elastic"
ENV ELASTICSEARCH_PASSWORD "elastic"
```
- Se define el usuario y password para el acceso a los indices del server elasticsearch (Se recomienda cambiar el password)

***
_Exponer puerto de docker:_

```
EXPOSE 5601
```
- Se expone el puerto de escucha del servicio de kibana.

***

1. instalación de git

```
$sudo apt install git
```

2. Clonar repositorio

```
$sudo git clone git@gitlab.com:adrian.guirao/elk
```

3. construimos las imagenes a partir de los dockerfiles
```
$docker build . -f elasticsearch -t elasticsearch
$docker build . -f kibana -t kibana
```

4. Iniciamos los dockers
```
$sudo docker run --network host -d --name elasticsearch elasticsearch 
$sudo docker run --network host -d --name kibana kibana
```
Una vez iniciado los docker, luego de uno o dos minutos podremos ingresar a la aplicación Kibana, ingresando a la url http://x.x.x.x:5601 (x.x.x.x corresponde a la ip de la vm donde se ejecuto el contenedor)

5. Configuración de Beats

Para poder enviar logs hacia EK deberemos configurar beats hacia elasticsearch. La configuración se puede realizar con los siguientes instructivos: 

Beats para entornos Windows: https://www.elastic.co/guide/en/beats/winlogbeat/current/winlogbeat-installation-configuration.html

Beats para entornos Linux: https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-installation-configuration.html


## Scripts

Dentro del proyecto se encuentran 4 scripts en bash para iniciar los dockers y actualizar las imagenes desde git.
```
**startek.sh**
Inicia los docker con un tiempo de demora de 10 segundos para kibana, ya que para que inicie elasticsearch debe estar completamente iniciado.
```
```
**updateelastic.sh**
Detiene los dockers, elimina el docker de elastic, realiza un pull del git y construye nuevamente la imagen.
```
```
**updatekibana.sh**
Detiene los dockers, elimina el docker de kibana, realiza un pull del git y construye nuevamente la imagen.
```
```
**update-ek.sh**
Actualiza ambas imagenes. Detiene los dockers, los elimina, realiza un pull en git, construye las imagenes y por ultimo las inicia.
```
