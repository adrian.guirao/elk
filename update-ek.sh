#!/bin/bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker image rm elasticsearch
docker image rm kibana
git pull
docker build . -f elasticsearch -t elasticsearch
docker build . -f kibana -t kibana
sudo docker run --network host -d --name elasticsearch elasticsearch 
sleep 10
sudo docker run --network host -d --name kibana kibana
