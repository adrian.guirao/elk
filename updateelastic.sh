#!/bin/bash
docker stop $(docker ps -a -q)
docker rm elasticsearch
docker image rm elasticsearch
git pull
docker build . -f elasticsearch -t elasticsearch
