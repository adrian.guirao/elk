#!/bin/bash
docker stop $(docker ps -a -q)
docker rm kibana
docker image rm kibana
git pull
docker build . -f kibana -t kibana
